<?php
$db_file = '/var/www/html/database.db';
/**
 * $statement = $dbh->prepare("select * from users where email = ?");
 * $statement->execute(array(email));
 */
try {
    $conn = new PDO("sqlite:$db_file");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    /* LIS le tableau */
    $user = $_POST['username'];
    $pass = $_POST['password'];
    /* prepare remplace les ? qu'il trouve par les éléments du tableau */
    $sql = "SELECT * FROM users WHERE username = ? AND password = ?";

    /* On remplace query (une méthode simple et directe) par prepare(qui prépare une requête) */
    $stmt = $conn->prepare($sql);
    /* lance la fonction, vérifie les caractères étranges et remplace chaque ? un à un par les éléments du tableau */
    $stmt->execute(array($user, $pass));

    if ($stmt === false) {
        echo "Error executing the query.<br>";
    } else {
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $rowCount = count($rows);

        if ($rowCount > 0) {
            echo "Login successful!<br>";
            foreach ($rows as $row) {
                echo "id: " . $row["id"]. " - Username: " . $row["username"]. " - Password: " . $row["password"]. "<br>";
            }
        } else {
            echo "Incorrect username or password.";
        }
    }
} catch (PDOException $e) {
    echo "Connection error: " . $e->getMessage();
}
